package com.pme.services;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;

import org.hibernate.Session;

import com.pme.entity.Type;
import com.pme.util.HibernateUtil;


@Named
@ApplicationScoped
public class ReferentielService {
	
	private List<Type> listType = new ArrayList<Type>();
	private List<Type> listTypeTemp = new ArrayList<Type>();
	private List<Type> listTypeTemp2 = new ArrayList<Type>();
	
	
	//*************CHARGEMENT AU DEMARRAGE DE L'APP*************************************
	public void init(@Observes @Initialized(ApplicationScoped.class) Object init){
		chargerType();
	}
	
	@SuppressWarnings("unchecked")
	public List<Type> returnListContext(String context){
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	List<Type> list = session.createQuery("select distinct e from Type e left outer join fetch e.listTypeEnfant where e.context = :param order by e.libelle")
    			.setParameter("param", context).list();
    	session.beginTransaction().commit();
    	
    	return list;
	}
	
	@SuppressWarnings("unchecked")
	public void chargerType(){
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	setListType(session.createQuery("from Type e order by e.libelle").list());
    	session.beginTransaction().commit();
	}
	

	
	public Type returnTypeByCode(String code){
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	Type type = (Type) session.createQuery("from Type e where e.code = :param").setParameter("param", code).uniqueResult();
    	session.beginTransaction().commit();
		return type;
	}
	
	
	
	public Type returnTypeByCode(String code,Session session){
    	
    	Type type = (Type) session.createQuery("from Type e where e.code = :param").setParameter("param", code).uniqueResult();
		return type;
	}
	
	public Type returnTypeByLibelle(String code){
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	Type type = (Type) session.createQuery("from Type e where e.libelle like :param").setParameter("param", "%"+code+"%").uniqueResult();
    	session.beginTransaction().commit();
    	return type;
	}
	
	public Type returnTypeById(Long id){
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	Type type = (Type) session.get(Type.class, id);
    	session.beginTransaction().commit();
		return type;
	}
	
/*	public Dossier returnDossierById(Long id){
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	Dossier dossier = (Dossier) session.get(Dossier.class, id);
    	session.beginTransaction().commit();
		return dossier;
	}*/
	

	
	@SuppressWarnings("unchecked")
	public void chargerQuartierDeVille(Type ville){
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	listTypeTemp = session.createQuery("from Type e where e.typeParent = :param order by e.libelle").setParameter("param", ville).list();
    	session.beginTransaction().commit();
    	
	}
	
	@SuppressWarnings("unchecked")
	public void typeChangeMethod(ValueChangeEvent e){
		Type type = (Type)e.getNewValue();
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	listTypeTemp2 = session.createQuery("from Type e where e.typeParent = :param order by e.libelle").setParameter("param", type).list();
    	session.beginTransaction().commit();
    	
	}
	
	//***********************GETTER SETTER**************************************************

	public List<Type> getListType() {
		return listType;
	}


	public void setListType(List<Type> listType) {
		this.listType = listType;
	}

	public List<Type> getListTypeTemp() {
		return listTypeTemp;
	}

	public void setListTypeTemp(List<Type> listTypeTemp) {
		this.listTypeTemp = listTypeTemp;
	}

	public List<Type> getListTypeTemp2() {
		return listTypeTemp2;
	}

	public void setListTypeTemp2(List<Type> listTypeTemp2) {
		this.listTypeTemp2 = listTypeTemp2;
	}





}
