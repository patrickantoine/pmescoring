package com.pme.services;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class PmeService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
	
	//*******************METHODES*******************************
	public String versConstantes(){
		return "/private/constantessytem.xhtml?faces-redirect=true";
	}
	
	public String versPortefeuilles(){
		return "/private/portefeuilles.xhtml?faces-redirect=true";
	}
	
	public String versMonportefeuille(){
		return "/private/portefeuilles.xhtml?faces-redirect=true";
	}
	
	public String versRegistrepme(){
		return "/private/registrepme.xhtml?faces-redirect=true";
	}
	
	public String versReporting(){
		return "/private/reporting.xhtml?faces-redirect=true";
	}
	
	
	
	
	//******************GETTER SETTER***************************

}
