package com.pme.services;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class ProjetService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String menucourant = "1_1";

	//***********************METHODES************************************************
	
	public String versHome(){
		
		return "/private/home.xhtml?faces-redirect=true";
	}
	//***********************GETTER SETTER*******************************************
	public String getMenucourant() {
		return menucourant;
	}

	public void setMenucourant(String menucourant) {
		this.menucourant = menucourant;
	}

} 
