package com.pme.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.pme.entity.ProfilRole;
import com.pme.entity.Type;
import com.pme.entity.Utilisateur;
import com.pme.util.FonctionsUtils;
import com.pme.util.HibernateUtil;
import com.pme.util.Login;
import com.pme.util.SimpleCrypto;

@Named
@SessionScoped
public class UtilisateurService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Login login;
	
	@Inject
	private ReferentielService referentielService;
	
	private TreeNode arbreRole;
	private TreeNode[] roleChoisi;	
	private Utilisateur utilisateur = null;
	private List<Utilisateur> list = null;
	private Type profil = null;
	private List<Type> listProfil = null;
	
	public void init(){
		utilisateur = new Utilisateur();
		utilisateur.setDateCreation(new Date());
		utilisateur.setUserCreation(login.getUserConnecte());
	}
	
	public void initProfil(){
		profil = new Type();
		profil.setContext("PROFIL");
		profil.setListRole(new ArrayList<ProfilRole>());
		initArbreRole();
	}
	
	@SuppressWarnings("unchecked")
	public void charger(){
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	setList(
    			session.createQuery("from Utilisateur e "
    			+ "order by e.nom")
    			.list()
    		);
    	session.beginTransaction().commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerProfil(){
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	setListProfil(
    			session.createQuery("from Type e "
    			+ "where e.context = 'PROFIL' "
    			+ "order by e.libelle")
    			.list()
    		);
    	session.beginTransaction().commit();
	}
	
	public void save(){
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	session.save(utilisateur);
    	session.beginTransaction().commit();
	}
	
	public void enregistrerUser() throws Exception{
		utilisateur.setMdp(SimpleCrypto.encrypt(utilisateur.getLogin(), "00000000"));
		save();
		init();
		charger();
		FonctionsUtils.popupSuccess("Utilisateur cr��...");
	}
	

	
	public void update(){
		utilisateur.setUserModif(login.getUserConnecte());
		utilisateur.setDateModif(new Date());
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	session.merge(utilisateur);
    	session.beginTransaction().commit();
	}
	
	public void modifierUser(){
		update();
		FonctionsUtils.popupSuccess("Utilisateur modifi�...");
	}
	
	public void updatePassAdmin() throws Exception{
		
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	Utilisateur user = (Utilisateur) session.get(Utilisateur.class, 0l);
    	user.setMdp(SimpleCrypto.encrypt(user.getLogin(), "00000000"));
    	session.merge(user);
    	session.beginTransaction().commit();
	}
	
	public void updatePassUserConnected() throws Exception{
		
    	login.getUserConnecte().setMdp(SimpleCrypto.encrypt(login.getUserConnecte().getLogin(), login.getUserConnecte().getMdp1()));
    	login.getUserConnecte().setDateModif(new Date());
		
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	session.merge(login.getUserConnecte());
    	session.beginTransaction().commit();
    	
    	FonctionsUtils.popupSuccess("Votre mot de passe a �t� modifi�...");
	}
	
	public void bloqueUser(Utilisateur us){
		this.setUtilisateur(us);
		utilisateur.setActif(false);
		update();
		FonctionsUtils.popupSuccess("Compte utilisateur v�rrouill�...");
	}
	
	public void activeUser(Utilisateur us){
		this.setUtilisateur(us);
		utilisateur.setActif(true);
		update();
		FonctionsUtils.popupSuccess("Compte utilisateur r�activ�...");
	}
	
	public void reinitialiserPassUser(Utilisateur us) throws Exception{
		this.setUtilisateur(us);
		utilisateur.setMdp(SimpleCrypto.encrypt(utilisateur.getLogin(), "00000000"));
		update();
		FonctionsUtils.popupSuccess("Mot de passe r�initialis� � 00000000...");
	}
	
	public void reinitialiserPassAdmin() throws Exception{
		Utilisateur adminUser = new Utilisateur();
		adminUser.setId(0l);
		adminUser.setMdp(SimpleCrypto.encrypt(adminUser.getLogin(), "00000000"));

    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	session.merge(adminUser);
    	session.beginTransaction().commit();
		
		FonctionsUtils.popupSuccess("Mot de passe r�initialis� � 00000000...");
	}
	
    public void initArbreRole() {
    	arbreRole = new DefaultTreeNode("ROLES", null);
    	for(Type uo : referentielService.returnListContext("MENU")) {
    		if(uo.getTypeParent() == null) {
    			TreeNode node0 = new DefaultTreeNode(uo, arbreRole);
        		creerBrancheArbre(uo,node0);
    		}
    	}
    }
    
    @SuppressWarnings("unchecked")
	public void creerBrancheArbre(Type un,TreeNode root) {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	
    	List<Type> listSOU = session.createQuery("from Type u "
    			+ "left outer join fetch u.typeParent rp "
    			+ "where u.typeParent = :param ")
    			.setParameter("param", un)
    			.list();
    	session.beginTransaction().commit();
    	if(listSOU.size() > 0){
    	
    		for(Type u :listSOU){
    			TreeNode node0 = new DefaultTreeNode(u, root);
    			creerBrancheArbre(u,node0);
    		}
    	}
    }
    
    
	public void enregistrerProfil(){
		try{
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	for(int u=0; u < roleChoisi.length; u++){
	    		ProfilRole pr = new ProfilRole();
	    		pr.setMenu((Type) roleChoisi[u].getData());
	    		profil.getListRole().add(pr);
	    	}
	    	session.save(profil);
	    	trans.commit();
	    	
	    	initProfil();
	    	chargerProfil();
	    	FonctionsUtils.popupSuccess("Profil enregistr�...");
		}catch(Exception e){
			e.printStackTrace();
			FonctionsUtils.popupErreur("Une erreur est survenue durant la cr�ation du profil...");
		}
	}
	
    public void chargerArbreRole() {
    	ProfilRole lerole = null;
    	arbreRole = new DefaultTreeNode("ROLES", null);
    	List<Type> listMenu = referentielService.returnListContext("MENU");
    	
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction().begin();
    	
    	for(Type uo : listMenu) {
    		if(uo.getTypeParent() == null) {
    			TreeNode node0 = new DefaultTreeNode(uo, arbreRole);
    			lerole = (ProfilRole) session.createQuery("from ProfilRole pr "
    					+ "left outer join fetch pr.menu r "
    					+ "where pr.profil = :param1 "
    					+ "and pr.menu = :param2")
    					.setParameter("param1", profil)
    					.setParameter("param2", uo).uniqueResult();
    			if(lerole != null) {
    				node0.setSelected(true);
    			}
    			chargerBrancheArbreSession(uo,node0,session);
    		}
    	}
    	
    	session.beginTransaction().commit();
    }
    
    @SuppressWarnings("unchecked")
	public void chargerBrancheArbreSession(Type un,TreeNode root,Session session ) {
    	
    	ProfilRole lerole = null;
    	
    	List<Type> listSOU = session.createQuery("from Type u "
    			+ "where u.typeParent = :param ")
    			.setParameter("param", un)
    			.list();
    	
    	if(listSOU.size() > 0){
    	
    		for(Type u :listSOU){
    			TreeNode node0 = new DefaultTreeNode(u, root);
				lerole = (ProfilRole) session.createQuery("from ProfilRole pr where pr.profil = :param1 and pr.menu = :param2")
						.setParameter("param1", profil)
						.setParameter("param2", u).uniqueResult();
				if(lerole != null) {
					node0.setSelected(true);
				}
				chargerBrancheArbreSession(u,node0,session);
    		}
    	}
    }  
    
    
	public void updateProfil(){
		try{
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	session.createQuery("delete ProfilRole pr where pr.profil = :param").setParameter("param", profil).executeUpdate();
	    	
	    	profil.setListRole(new ArrayList<ProfilRole>());
	    	
	    	for(int u=0; u < roleChoisi.length; u++){
	    		ProfilRole pr = new ProfilRole();
	    		pr.setMenu((Type) roleChoisi[u].getData());
	    		profil.getListRole().add(pr);
	    	}
	    	
	    	session.merge(profil);
	    	
	    	trans.commit();
	    	initProfil();
	    	charger();
	    	FonctionsUtils.popupSuccess("Profil modifi�...");
		}catch(Exception e){
			e.printStackTrace();
			FonctionsUtils.popupErreur("Une erreur est survenue durant la cr�ation du profil...");
		}
	}
	
	public String versUtilisateur(){
		init();
		charger();
		return "/private/users.xhtml?faces-redirect=true";
	}	
	
	public String versProfil(){
		initProfil();
		chargerProfil();
		initArbreRole();
		return "/private/profils.xhtml?faces-redirect=true";
	}	
	
	public String versSetting(){
		
		return "/private/setting.xhtml?faces-redirect=true";
	}
	
	public Boolean isAutorized(String menu){
		boolean result = false;
		
		if(login.getRoles().contains(menu)){
			result = true;
		}
		
		return result;
	}
	
	//**********************GETTER SETTER*************************************
	public TreeNode getArbreRole() {
		return arbreRole;
	}
	public void setArbreRole(TreeNode arbreRole) {
		this.arbreRole = arbreRole;
	}
	public TreeNode[] getRoleChoisi() {
		return roleChoisi;
	}
	public void setRoleChoisi(TreeNode[] roleChoisi) {
		this.roleChoisi = roleChoisi;
	}
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	public List<Utilisateur> getList() {
		return list;
	}
	public void setList(List<Utilisateur> list) {
		this.list = list;
	}

	public Type getProfil() {
		return profil;
	}

	public void setProfil(Type profil) {
		this.profil = profil;
	}

	public List<Type> getListProfil() {
		return listProfil;
	}

	public void setListProfil(List<Type> listProfil) {
		this.listProfil = listProfil;
	}
	

}
