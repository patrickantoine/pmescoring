package com.pme.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;


import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@SessionScoped
@Named("printService")
public class PrintService implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static StreamedContent streamedContent;
	
	private static InputStream inputStream = null;
	
	@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
/*	public static void genererFiche(Map<String,Object> autreParams,String nomFichier, List<FactureDetail> list) throws Exception{
	
		ExternalContext ec =	FacesContext.getCurrentInstance().getExternalContext();
		ServletContext sc = (ServletContext)ec.getContext();
		
		String path = sc.getRealPath("/private/etats/");
		String fichierJRXML = path+"/fichesoin.jrxml";
		
		InputStream inputStream = new FileInputStream(fichierJRXML);
		JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
		JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
		
		HashMap parameters = new HashMap();
		parameters.put("LOGO",path+"/logo.png");
		
        if(autreParams!= null && !autreParams.isEmpty()){
     	   for(Entry<String, Object> entry : autreParams.entrySet()) {
     		    String cle = entry.getKey();
     		    Object valeur = entry.getValue();
     		    
     		    parameters.put(cle, valeur);
     		}
        }
		
		JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(list);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,beanCollectionDataSource);
		
        // - Cr�ation du rapport au format PDF
        byte[] docPdf = JasperExportManager.exportReportToPdf(jasperPrint);
        
        InputStream is = new ByteArrayInputStream(docPdf);
        inputStream = is;
        streamedContent = new DefaultStreamedContent(is, "application/pdf",nomFichier);
           
	}*/
	
	
	
/*	public static void genererDeclaration(Map<String,Object> autreParams,List<DTOfiche> list, String numero) throws FileNotFoundException, JRException{
		
		ExternalContext ec =	FacesContext.getCurrentInstance().getExternalContext();
		ServletContext sc = (ServletContext)ec.getContext();
		
		String path = sc.getRealPath("/private/etats/");
		String fichierJRXML = path+"/fiche_declaration.jrxml";
		
		InputStream inputStream = new FileInputStream(fichierJRXML);
		JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
		JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
		
		HashMap parameters = new HashMap();
		parameters.put("LOGO",path+"/logo.png");
		
        if(autreParams!= null && !autreParams.isEmpty()){
     	   for(Entry<String, Object> entry : autreParams.entrySet()) {
     		    String cle = entry.getKey();
     		    Object valeur = entry.getValue();
     		    
     		    parameters.put(cle, valeur);
     		}
        }
		
		JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(list);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,beanCollectionDataSource);
		
        // - Cr�ation du rapport au format PDF
        byte[] docPdf = JasperExportManager.exportReportToPdf(jasperPrint);
        
        InputStream is = new ByteArrayInputStream(docPdf);
        inputStream = is;
        streamedContent = new DefaultStreamedContent(is, "application/pdf","DECLARATION "+numero+".pdf");
		
	}*/

	public StreamedContent getStreamedContent() {
		return streamedContent;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	
}

