package com.pme.util;

import java.security.Key;
import java.util.Date;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.pme.entity.Utilisateur;

public class LoginDAO {
	
	
	@SuppressWarnings("unchecked")
	public static Utilisateur validate(String user, String password) {

		Utilisateur userConnecte = null;

    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	userConnecte = (Utilisateur) session.createQuery("from Utilisateur u "
    			+ "left outer join fetch u.profil pr "
    			+ "left outer join fetch pr.listRole listprofil "
    			+ "left outer join fetch listprofil.menu "
    			+ "where u.login = :paramUser "
    			+ "and u.mdp = :paramPass "
    			)
    			.setParameter("paramUser", user)
    			.setParameter("paramPass", password)
    			.uniqueResult();
    	
    	if(userConnecte!=null){
    		userConnecte.setDerniereConnexion(new Date());
    		session.update(userConnecte);
    	}
    	trans.commit();
		return userConnecte;
		
	}
	

	
	

	
}