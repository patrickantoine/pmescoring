package com.pme.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;



@Named
@SessionScoped
public class DownloadService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//@Inject
	//private ReferentielService referentielService;
	
	public void downloadFile(String filename) throws IOException {
		
		try{
			FacesContext context = FacesContext.getCurrentInstance();
			ExternalContext externalContext = context.getExternalContext();
			
			externalContext.responseReset();
			externalContext.setResponseContentType("application/octet-stream");
			externalContext.setResponseHeader("Content-Disposition", "attachement;filename="+filename);
			
			//System.out.println("------------------"+(String)referentielService.returnConstante("REP_PC"));
			
			FileInputStream inputStream = new FileInputStream(new File("C:\\webapp_file\\scan\\"+filename));
			OutputStream outputStream = externalContext.getResponseOutputStream();
			
			byte[] buffer = new byte[1024];
			int length;
			while((length = inputStream.read(buffer)) > 0) {
				outputStream.write(buffer, 0, length);
			}
			
			inputStream.close();
			context.responseComplete();
		}catch(Exception e){
			FonctionsUtils.popupErreurSansReload("Impossible de t�l�charger le document...");
		}

	}
	
	
	public void supprimerFichier(String filename){
		try{

		      File file = new File("C:\\webapp_file\\scan\\"+filename);
		      file.delete();
		      //if(file.delete()){
		       //System.out.println(file.getName() + " est supprim�.");
		     // }else{
		       //System.out.println("Op�ration de suppression echou�e");
		      //}
		      
		      /*File dossier = new File("c:\\dossier_log");

		      if(dossier.delete()){
		       System.out.println(dossier.getName() + " est supprim�.");
		      }else{
		       System.out.println("Op�ration de suppression echou�e");
		      }*/

		 }catch(Exception e){
		      e.printStackTrace();
		 }
	}

}
