package com.pme.util;

import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.*;

import com.pme.entity.ProfilRole;
import com.pme.entity.Referentiel;
import com.pme.entity.Type;
import com.pme.entity.Utilisateur;



public class HibernateUtil {
	private static SessionFactory sessionFactory;
	
    static {
    	
    	try {
    		Configuration configuration = new Configuration().configure();
			
			configuration.addAnnotatedClass(Utilisateur.class);
			configuration.addAnnotatedClass(Referentiel.class);
			configuration.addAnnotatedClass(Type.class);
			configuration.addAnnotatedClass(ProfilRole.class);
    		
    		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
    		sessionFactory = configuration.buildSessionFactory(builder.build());
        } catch (Throwable ex) {
            // Gestion exception
            System.err.println("Echec cr�ation SessionFactory" + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    // Call this during shutdown
    public static void close() {
    	sessionFactory.close();
    }


}
