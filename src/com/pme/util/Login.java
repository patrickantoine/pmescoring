package com.pme.util;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.pme.entity.ProfilRole;
import com.pme.entity.Utilisateur;
import com.pme.services.UtilisateurService;


@Named("login")
@SessionScoped
public class Login implements Serializable {

	private static final long serialVersionUID = 1094801825228386363L;
	
	@Inject
	private UtilisateurService utilisateurService;

	
	private String pwd;
	private String msg;
	private String user;
	private Utilisateur userConnecte;
	private List<String> roles;
	private String test;

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String validateUsernamePassword() throws Exception{
		HttpSession session = (HttpSession) SessionUtils.getSession();
		userConnecte = LoginDAO.validate(user,SimpleCrypto.encrypt(user, pwd) );
		//userConnecte = LoginDAO.validate(user, pwd );
		String redirect = "";
		if (userConnecte != null) {
			
			if(userConnecte.getActif()){
				roles = new ArrayList<String>();
				for(ProfilRole ipr : userConnecte.getProfil().getListRole()){
					roles.add(ipr.getMenu().getCode());
				}
				
				session.setAttribute("username", user);
				session.setAttribute("AUTHENTICATED", true);
				redirect = "/private/home.xhtml?faces-redirect=true";
			}else{
				FonctionsUtils.popupErreur("Compte bloqu�...");
			}

		} else {
			FonctionsUtils.popupErreur("Login ou mot de passe incorrect...");
		}
		
		return redirect;
	}

	public String logout() {
		HttpSession session = (HttpSession) SessionUtils.getSession();
		session.invalidate();
		
		String redirect = "/index.xhtml?faces-redirect=true";
		
		return redirect;
	}

	public Utilisateur getUserConnecte() {
		return userConnecte;
	}

	public void setUserConnecte(Utilisateur userConnecte) {
		this.userConnecte = userConnecte;
	}

	/**
	 * @return the test
	 */
	public String getTest() {
		return test;
	}

	/**
	 * @param test the test to set
	 */
	public void setTest(String test) {
		this.test = test;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

}
