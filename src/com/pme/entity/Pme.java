package com.pme.entity;

import java.io.Serializable;

public class Pme implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long libelle;
	private Long code;
	private String rccm;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getLibelle() {
		return libelle;
	}
	public void setLibelle(Long libelle) {
		this.libelle = libelle;
	}
	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
	public String getRccm() {
		return rccm;
	}
	public void setRccm(String rccm) {
		this.rccm = rccm;
	}

}
