package com.pme.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

@Entity
public class Utilisateur implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String login;
	private String mdp;
	private Date derniereConnexion;
	private Date dateCreation;
	private Date dateModif;
	private Utilisateur userCreation;
	private Utilisateur userModif;
	private Type profil;
	private String mdp1;
	private String mdp2;
	private Boolean actif = true;
	private String nom;
	private String prenom;
	private String mail;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
	@SequenceGenerator(name="user_generator", sequenceName = "user_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public Date getDerniereConnexion() {
		return derniereConnexion;
	}
	public void setDerniereConnexion(Date derniereConnexion) {
		this.derniereConnexion = derniereConnexion;
	}
	
	@OneToOne
	@JoinColumn(name = "idusercreation", nullable=true)
	public Utilisateur getUserCreation() {
		return userCreation;
	}
	public void setUserCreation(Utilisateur userCreation) {
		this.userCreation = userCreation;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	@OneToOne
	@JoinColumn(name = "idusermodif", nullable=true)
	public Utilisateur getUserModif() {
		return userModif;
	}
	public void setUserModif(Utilisateur userModif) {
		this.userModif = userModif;
	}
	public Date getDateModif() {
		return dateModif;
	}
	public void setDateModif(Date dateModif) {
		this.dateModif = dateModif;
	}
	
	@OneToOne
	@JoinColumn(name = "idprofil", nullable=true)
	public Type getProfil() {
		return profil;
	}
	public void setProfil(Type profil) {
		this.profil = profil;
	}
	@Transient
	public String getMdp1() {
		return mdp1;
	}
	public void setMdp1(String mdp1) {
		this.mdp1 = mdp1;
	}
	@Transient
	public String getMdp2() {
		return mdp2;
	}
	public void setMdp2(String mdp2) {
		this.mdp2 = mdp2;
	}
	public Boolean getActif() {
		return actif;
	}
	public void setActif(Boolean actif) {
		this.actif = actif;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	

}
