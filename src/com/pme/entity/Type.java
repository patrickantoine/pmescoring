package com.pme.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
public class Type implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String libelle;
	private String code;
	private String context;
	private Type typeParent;
	private Boolean coche = false;
	private List<Type> listTypeEnfant;
	private List<ProfilRole> listRole;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "type_generator")
	@SequenceGenerator(name="type_generator", sequenceName = "type_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * @return the typeParent
	 */
	@OneToOne
	@JoinColumn(name = "idparent", nullable=true)
	public Type getTypeParent() {
		return typeParent;
	}
	/**
	 * @param typeParent the typeParent to set
	 */
	public void setTypeParent(Type typeParent) {
		this.typeParent = typeParent;
	}
	/**
	 * @return the coche
	 */
	@Transient
	public Boolean getCoche() {
		return coche;
	}
	/**
	 * @param coche the coche to set
	 */
	public void setCoche(Boolean coche) {
		this.coche = coche;
	}
	
	/**
	 * @return the listTypeEnfant
	 */
	@OneToMany
	@JoinColumn(name="idparent",nullable=true)
	@Cascade(value=(CascadeType.ALL))
	public List<Type> getListTypeEnfant() {
		return listTypeEnfant;
	}
	/**
	 * @param listTypeEnfant the listTypeEnfant to set
	 */
	public void setListTypeEnfant(List<Type> listTypeEnfant) {
		this.listTypeEnfant = listTypeEnfant;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Type other = (Type) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		return true;
	}
	@OneToMany
	@JoinColumn(name="idprofil",nullable=true)
	@Cascade(value=(CascadeType.ALL))
	public List<ProfilRole> getListRole() {
		return listRole;
	}
	public void setListRole(List<ProfilRole> listRole) {
		this.listRole = listRole;
	}
	

}
