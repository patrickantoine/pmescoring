package com.pme.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class ProfilRole implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Type profil;
	private Type menu;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "profilrole_generator")
	@SequenceGenerator(name="profilrole_generator", sequenceName = "profilrole_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@OneToOne
	@JoinColumn(name = "idprofil", nullable=true)
	public Type getProfil() {
		return profil;
	}
	public void setProfil(Type profil) {
		this.profil = profil;
	}
	@OneToOne
	@JoinColumn(name = "idmenu", nullable=true)
	public Type getMenu() {
		return menu;
	}
	public void setMenu(Type menu) {
		this.menu = menu;
	}

}
