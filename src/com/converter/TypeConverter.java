package com.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.pme.entity.Type;
import com.pme.services.ReferentielService;




@FacesConverter("typeConverter")
public class TypeConverter implements Converter {

	  @Override
	    public Object getAsObject(FacesContext ctx, UIComponent uiComponent, String Id) {
	        ValueExpression vex =
	                ctx.getApplication().getExpressionFactory()
	                        .createValueExpression(ctx.getELContext(),
	                                "#{referentielService}", ReferentielService.class);

	        ReferentielService referentielService = (ReferentielService)vex.getValue(ctx.getELContext());
	        try{
	        	//System.out.println("id------------"+Id);
	        	return referentielService.returnTypeById(new Long(Id));
	        }catch(Exception e){
	        	return null;
	        }
	    }

	    @Override
	    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object objet) {
	    	try{
	    		
	    		//System.out.println("objet------------"+((Type)objet).getId().toString());
	    		return ((Type)objet).getId().toString();
	    	}catch(Exception e){
	    		return null;
	    	}
	    }
}
